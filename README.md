Community BnB is a proof of concept... An Airbnb for displaced victims of natural disasters and the volunteers who'd like to open up their homes for free.<br>
For future development, I'd like to incorporate Google OAuth as a way to sign in securely.  I'd also like to continue building on to the home seekers side of the app.


Deployed App: https://communitybnb.herokuapp.com

Technologies Used: Trello, Heroku, MongoDB, Mongoose, Express, Node.js, React, HTML, Bootstrap, CSS, Google and lots of prayer.

